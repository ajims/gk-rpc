package com.smgeek.gkrpc.transport.TransportServiceImpl;

import com.smgeek.gkrpc.Peer;
import com.smgeek.gkrpc.transport.TransportClient;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPTransportClient implements TransportClient {

    private String url;

    @Override
    public void connect(Peer peer) {
        //基于短链接
        this.url = "http://" + peer.getHost() + ":" + peer.getPort();
    }

    @Override
    public InputStream write(InputStream data) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();

            //https://blog.csdn.net/u010665691/article/details/45558119
            //创建连接的设置
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.connect();

            //将data发送出去
            IOUtils.copy(data,conn.getOutputStream());

            int resultCode = conn.getResponseCode();
            if (resultCode == HttpURLConnection.HTTP_OK){
                return conn.getInputStream();
            }else {
                return conn.getErrorStream();
            }


            //httpUrlConnection 没有close()方法
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void close() {

    }
}
