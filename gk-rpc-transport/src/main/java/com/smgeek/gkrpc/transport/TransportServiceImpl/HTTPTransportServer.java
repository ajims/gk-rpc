package com.smgeek.gkrpc.transport.TransportServiceImpl;

import com.smgeek.gkrpc.transport.RequestHandle;
import com.smgeek.gkrpc.transport.TransportServe;
import com.sun.media.jfxmedia.logging.Logger;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.Holder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Slf4j
public class HTTPTransportServer implements TransportServe {

    private RequestHandle handle;
    private Server server;

    @Override
    public void init(int port, RequestHandle handle) {

        this.handle = handle;
        this.server = new Server(port);

        //servlet
        ServletContextHandler ctx = new ServletContextHandler();
        server.setHandler(ctx);

        ServletHolder Holder = new ServletHolder(new RequestServlet());
        ctx.addServlet(Holder, "/*");
    }

    @Override
    public void start() {
        try {
            server.start();
            server.join();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    @Override
    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }


    class RequestServlet extends HttpServlet {


        //处理POST请求
        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            log.info("client connect");
            InputStream in = req.getInputStream();
            OutputStream out = resp.getOutputStream();

            if (handle != null) {
                handle.onRequest(in, out);
            }

            out.flush();
        }
    }
}
