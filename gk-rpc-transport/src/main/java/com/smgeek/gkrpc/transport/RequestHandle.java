package com.smgeek.gkrpc.transport;


import java.io.InputStream;
import java.io.OutputStream;

/*
* 将InputStream 转换为 可以处理的 数据类型
* 处理网络请求的handle
* */
public interface RequestHandle {

    void onRequest(InputStream revice, OutputStream toResp);
}
