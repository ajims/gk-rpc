package com.smgeek.gkrpc.transport;


import com.smgeek.gkrpc.Peer;

import java.io.InputStream;

/*
* 1.启动,监听端口
* 2.接受请求
* 3.关闭端口
* */
public interface TransportServe {
    void init(int port,RequestHandle handle);

    void start();
    void stop();
}
