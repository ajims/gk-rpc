package com.smgeek.gkrpc;

import lombok.Data;
/*
* 表示rpc的一个请求
* */
@Data
public class Request {

    private ServiceDescriptor service;

    //数组的类型是不确定的,所以使用object[]类型的数组
    private Object[] parameter;
}
