package com.smgeek.gkrpc.common.utils;

import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class ReflectionUtilsTest {

    @Test
    public void newInstance() {
        TestClassic t = ReflectionUtils.newInstance(TestClassic.class);
        assertNotNull(t);
    }

    @Test
    public void getPublicMethods() {

        Method[] methods = ReflectionUtils.getPublicMethods(TestClassic.class);
        assertEquals(1,methods.length);

        String methodName = methods[0].getName();
        assertEquals("b",methodName);
    }

    @Test
    public void invoke() {
        Method[] methods = ReflectionUtils.getPublicMethods(TestClassic.class);
        Method b = methods[0];

        TestClassic t = new TestClassic();
        Object r = ReflectionUtils.invoke(t,b);

        assertEquals("b",r);
    }
}