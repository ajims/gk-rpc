package com.smgeek.gkrpc.common.utils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/*
* 反射工具类
* */
public class ReflectionUtils {
    //这个<T>表示这个方法声明为泛型方法


    /*
    * 根据class创建对象
    *
    * @param clazz 待创建对象的类
    * @param <T> 代表这是一个泛型类
    * @return  创建好的对象
    * */
    public static <T> T newInstance(Class<T> clazz){
        try {
            return clazz.newInstance();
        }catch (Exception e){
            throw new IllegalStateException(e);
        }
    }


    /*
    * 获取某个class的共有方法
    * */
    public static Method[] getPublicMethods(Class clazz){
        Method[] method = clazz.getDeclaredMethods();
        List<Method> methods = new ArrayList<>();
        for(Method meth :method){
            if (Modifier.isPublic(meth.getModifiers())){
                methods.add(meth);
            }
        }

        //入参数组长度小于真实长度，则自动扩容，所以可以使用Method[0]
        return methods.toArray(new Method[0]);
    }

    /*
    * 调用指定对象的指定方法
    * @param 被调用的指定对象
    * @param 被调用的方法,
    * @param 方法的参数
    * @return 返回结果
    * */
    public static Object invoke(Object obj,
                                Method method,
                                Object... args){
        try {
            //如果调用的方法是静态方法,则方法不属于对象而属于类,第一个参数则为NULL
            return method.invoke(obj,args);
        }catch (Exception e){
            throw new IllegalStateException(e);
        }

    }
}
