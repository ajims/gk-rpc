package com.smgeek.gkrpc.CodeAndDecode;

import lombok.Data;

@Data
public class TestBean {

    private String name;
    private int age;
}
