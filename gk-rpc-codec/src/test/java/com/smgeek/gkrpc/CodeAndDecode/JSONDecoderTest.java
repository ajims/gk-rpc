package com.smgeek.gkrpc.CodeAndDecode;

import com.smgeek.gkrpc.Decode;
import com.smgeek.gkrpc.Encodec;
import org.junit.Test;

import static org.junit.Assert.*;

public class JSONDecoderTest {

    @Test
    public void decode() {
        Encodec encoder = new JSONEncoder();

        TestBean tb = new TestBean();
        tb.setName("ajims");
        tb.setAge(24);

        byte[] bytes = encoder.encode(tb);

        Decode decode = new JSONDecoder();
        TestBean bean2 = decode.decode(bytes,TestBean.class);

        assertEquals(tb.getName(),bean2.getName());
        assertEquals(tb.getAge(),bean2.getAge());
    }
}