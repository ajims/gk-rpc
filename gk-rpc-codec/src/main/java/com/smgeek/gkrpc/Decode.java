package com.smgeek.gkrpc;

/*
 * 反序列化
 * */
public interface Decode {
    <T> T decode(byte[] bytes, Class<T> clazz);
}
