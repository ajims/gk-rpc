package com.smgeek.gkrpc.CodeAndDecode;

import com.alibaba.fastjson.JSON;
import com.smgeek.gkrpc.Decode;

/*
 * 基于JSON的反序列化实现
 * */
public class JSONDecoder implements Decode {
    @Override
    public <T> T decode(byte[] bytes, Class<T> clazz) {
        return JSON.parseObject(bytes, clazz);
    }
}
