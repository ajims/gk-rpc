package com.smgeek.gkrpc.CodeAndDecode;

/*
* 基于JSON的序列化实现
* */

import com.alibaba.fastjson.JSON;
import com.smgeek.gkrpc.Encodec;

public class JSONEncoder implements Encodec {

    @Override
    public byte[] encode(Object obj) {
        return JSON.toJSONBytes(obj);
    }
}
