package com.smgeek.gkrpc.example;

import com.smgeek.gkrpc.client.Impl.RpcClient;

public class Client {
    public static void main(String[] args) {
        RpcClient client = new RpcClient();
        CalcService service = client.getProxy(CalcService.class);

        int num1 = service.add(10,20);
        int num2 = service.minus(20,10);

        System.out.println(num1);
        System.out.println(num2);
    }
}
