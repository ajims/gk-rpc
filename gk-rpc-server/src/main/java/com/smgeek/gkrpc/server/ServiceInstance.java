package com.smgeek.gkrpc.server;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Method;

/*
* 表示一个具体服务
* */

@Data
@AllArgsConstructor
public class ServiceInstance {
    //两个属性,1.这个服务是由哪个对象提供的 2.这个对象有哪些方法

    private Object target;
    private Method methods;
}
