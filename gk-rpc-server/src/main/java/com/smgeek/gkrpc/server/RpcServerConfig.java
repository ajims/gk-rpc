package com.smgeek.gkrpc.server;

import com.smgeek.gkrpc.CodeAndDecode.JSONDecoder;
import com.smgeek.gkrpc.CodeAndDecode.JSONEncoder;
import com.smgeek.gkrpc.Decode;
import com.smgeek.gkrpc.Encodec;
import com.smgeek.gkrpc.transport.TransportServe;
import com.smgeek.gkrpc.transport.TransportServiceImpl.HTTPTransportServer;
import lombok.Data;

import javax.servlet.http.HttpServlet;

/*
 * Sever配置类
 * */
@Data
public class RpcServerConfig {

    //1.使用哪些网络模块 2.使用哪个序列化的实现 3.RpcServer启动后监听什么端口

    private Class<? extends TransportServe> transportClass = HTTPTransportServer.class;
    private Class<? extends Encodec> encoderClass = JSONEncoder.class;
    private Class<? extends Decode> decoderClass = JSONDecoder.class;
    private int port = 3000;
}
