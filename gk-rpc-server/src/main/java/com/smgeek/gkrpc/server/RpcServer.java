package com.smgeek.gkrpc.server;

import com.smgeek.gkrpc.Decode;
import com.smgeek.gkrpc.Encodec;
import com.smgeek.gkrpc.Request;
import com.smgeek.gkrpc.Response;
import com.smgeek.gkrpc.common.utils.ReflectionUtils;
import com.smgeek.gkrpc.transport.RequestHandle;
import com.smgeek.gkrpc.transport.TransportServe;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.util.IO;
import org.omg.IOP.Encoding;
import sun.misc.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Slf4j
public class RpcServer {
    //1.配置信息 2.网络模块 3.序列化和反序列化 4.ServiceManager

    private RpcServerConfig config;
    private TransportServe net;
    private Encodec encoding;
    private Decode decode;
    private ServiceManager serviceManager;
    private ServiceInvoker serviceInvoker;

    public RpcServer(){}

    public RpcServer(RpcServerConfig config) {
        this.config = config;

        //net
        this.net = ReflectionUtils.newInstance(config.getTransportClass());
        this.net.init(config.getPort(), this.handler);

        //encode
        this.encoding = ReflectionUtils.newInstance(config.getEncoderClass());

        //decode
        this.decode = ReflectionUtils.newInstance(config.getDecoderClass());

        //service
        this.serviceManager = new ServiceManager();
        this.serviceInvoker = new ServiceInvoker();
    }

    public <T> void register(Class<T> interfaceClass, T bean) {
        serviceManager.register(interfaceClass, bean);
    }

    public void start() {
        this.net.start();
    }

    public void stop() {
        this.net.stop();
    }

    private RequestHandle handler = new RequestHandle() {
        @Override
        public void onRequest(InputStream revice, OutputStream toResp) {
            //从revice 得到 request的数据体

            Response response = new Response();

            try {
                byte[] inByte = IOUtils.readNBytes(revice, revice.available());
                Request request = decode.decode(inByte, Request.class);
                log.info("get request:{}", request);

                ServiceInstance sis = serviceManager.lookup(request);

                Object res = serviceInvoker.invoke(sis, request);
                response.setData(res);

            } catch (IOException e) {
                log.warn(e.getMessage(), e);
                response.setCode(1);
                response.setMessage("RpcServer got error:"
                        + e.getClass().getName() + ":" + e.getMessage());
            } finally {
                //把数据返回到客户端
                try {
                    byte[] outBytes = encoding.encode(response);
                    toResp.write(outBytes);

                    log.info("response client");
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                }
            }
        }
    };
}
