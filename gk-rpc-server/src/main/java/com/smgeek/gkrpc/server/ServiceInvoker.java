package com.smgeek.gkrpc.server;


import com.smgeek.gkrpc.Request;
import com.smgeek.gkrpc.common.utils.ReflectionUtils;

/*
* 是用来调用Service实例的类
* 调用具体服务的
* */
public class ServiceInvoker {
    public Object invoke(ServiceInstance service,
                         Request request){
        return ReflectionUtils.invoke(
                service.getTarget(),
                service.getMethods(),
                request.getParameter()
        );
    }
}
