package com.smgeek.gkrpc.server;

import com.smgeek.gkrpc.Request;
import com.smgeek.gkrpc.ServiceDescriptor;
import com.smgeek.gkrpc.common.utils.ReflectionUtils;
import com.smgeek.gkrpc.server.Impl.TestServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.model.TestClass;

import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class ServiceManagerTest {

    ServiceManager sm;

    @Before
    //初始化方法,对于每个测试方法都要执行一遍
    public void init() {
        sm = new ServiceManager();

        TestInterface bean = new TestServiceImpl();
        sm.register(TestInterface.class, bean);
    }

    @Test
    public void register() {
        TestInterface bean = new TestServiceImpl();
        sm.register(TestInterface.class, bean);
    }

    @Test
    public void lookup() {
        Method method = ReflectionUtils.getPublicMethods(TestInterface.class)[0];

        ServiceDescriptor sdp = ServiceDescriptor.form(TestInterface.class, method);
        Request request = new Request();
        request.setService(sdp);

        ServiceInstance sis = sm.lookup(request);
        assertNotNull(sis);

    }
}