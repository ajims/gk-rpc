package com.smgeek.gkrpc.client;

import com.smgeek.gkrpc.Peer;
import com.smgeek.gkrpc.transport.TransportClient;
import sun.rmi.transport.Transport;

import java.util.List;

/*
* 表示选择哪个server去连接
* */
public interface TransportSelector {

    //1.selector方法,用于选择一个transport来进行交互

    //初始化
    // count 代表client和server建立多少个连接
    //client实现class
    void init(List<Peer> peers, int count,
              Class<?extends TransportClient> clazz);

    /*
    * 选择一个transport与server做连接
    * */
    TransportClient selector();

    /*
    * 释放用完的client
    * */
    void release(TransportClient client);

    void close();
}
