package com.smgeek.gkrpc.client.Impl;

import com.smgeek.gkrpc.*;
import com.smgeek.gkrpc.client.TransportSelector;
import com.smgeek.gkrpc.transport.TransportClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


/*
 * 调用远程服务代理类
 * */
@Slf4j
public class RemoteInvoke implements InvocationHandler {

    private Class clazz;
    private Encodec encoder;
    private Decode decoder;
    private TransportSelector selector;

    public RemoteInvoke(Class clazz,
                        Encodec encoder,
                        Decode decoder,
                        TransportSelector selector) {
        this.clazz = clazz;
        this.encoder = encoder;
        this.decoder = decoder;
        this.selector = selector;
    }

    ;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        //实现一个请求
        Request request = new Request();
        request.setService(ServiceDescriptor.form(clazz, method));
        request.setParameter(args);

        //通过网络传输调用远程服务,拿到response,判断,然后就可以结束网络传输
        Response rep = invokeRemote(request);

        if (rep == null || rep.getCode() != 0) {
            throw new IllegalStateException("fail to invoke remote:" + rep);
        }
        return rep.getData();
    }

    private Response invokeRemote(Request request) {

        Response resp = null;
        //1.拿到网络连接信息
        TransportClient client = null;

        try {
            client = selector.selector();

            //序列化,写入
            //错写成client
            byte[] outBytes = encoder.encode(request);
            InputStream revice = client.write(new ByteArrayInputStream(outBytes));

            //反序列化
            //available:可以不受阻塞地从此输入流读取（或跳过）的估计字节数；如果到达输入流末尾，则返回 0
            byte[] inBytes = IOUtils.readFully(revice, revice.available());
            resp = decoder.decode(inBytes, Response.class);


        } catch (IOException e) {
            log.warn(e.getMessage(), e);
            resp = new Response();
            resp.setCode(1);
            resp.setMessage("RpcClient got error:" + e.getMessage());
        } finally {
            if (client != null) {
                selector.release(client);
            }
        }
        return resp;
    }
}
