package com.smgeek.gkrpc.client.Impl;

import com.smgeek.gkrpc.Decode;
import com.smgeek.gkrpc.Encodec;
import com.smgeek.gkrpc.client.TransportSelector;
import com.smgeek.gkrpc.common.utils.ReflectionUtils;

import java.lang.reflect.Proxy;

public class RpcClient {

    private RpcClientConfig config;
    private Encodec encoder;
    private Decode decoder;
    private TransportSelector selector;

    public RpcClient() {
        //指向参数是该类型的构造方法
        this(new RpcClientConfig());
    }

    public RpcClient(RpcClientConfig config) {
        this.config = config;

        this.encoder = ReflectionUtils.newInstance(this.config.getEncoder());
        this.decoder = ReflectionUtils.newInstance(this.config.getDecoder());
        this.selector = ReflectionUtils.newInstance(this.config.getSelectorClass());

        this.selector.init(
                this.config.getServers(),
                this.config.getConnectCount(),
                this.config.getTransportClass()
        );
    }

    //获取一个接口的代理对象
    public <T> T getProxy(Class<T> clazz) {
        return (T) Proxy.newProxyInstance(
                getClass().getClassLoader(),
                new Class[]{clazz},
                new RemoteInvoke(clazz, encoder, decoder, selector)
        );
    }
}
