package com.smgeek.gkrpc.client.Impl;

import com.smgeek.gkrpc.CodeAndDecode.JSONDecoder;
import com.smgeek.gkrpc.CodeAndDecode.JSONEncoder;
import com.smgeek.gkrpc.Decode;
import com.smgeek.gkrpc.Encodec;
import com.smgeek.gkrpc.Peer;
import com.smgeek.gkrpc.client.TransportSelector;
import com.smgeek.gkrpc.transport.TransportClient;
import com.smgeek.gkrpc.transport.TransportServiceImpl.HTTPTransportClient;
import lombok.Data;

import java.util.Arrays;
import java.util.List;
@Data
public class RpcClientConfig {

    private Class<? extends TransportClient> transportClass =
            HTTPTransportClient.class;

    private Class<? extends Encodec> encoder = JSONEncoder.class;
    private Class<? extends Decode> decoder = JSONDecoder.class;
    private Class<?extends TransportSelector> selectorClass =
            RandomTransportSelector.class;
    private int connectCount =1;

    //可以连哪些网络端点
    private List<Peer> servers = Arrays.asList(
            new Peer("127.0.0.1",3000)
    );
}
